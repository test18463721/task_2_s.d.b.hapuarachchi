
public class AardvarkController {
  private AardvarkModel model;
  private AardvarkView view;

  public AardvarkController() {
    model = new AardvarkModel();
    view = new AardvarkView();
  }

  public void userInterfaceRun() {
    view.displayWelcomeMessage();
    String playerName = view.getPlayerName();

    // Perform game setup and initialization using the model

    int userChoice = -9;
    while (userChoice != 0) {
      userChoice = view.getMainMenuChoice();

      switch (userChoice) {
        case 1:
          // Play the game
          // Use the view to display the game interface and get user input
          // Use the model to handle game logic and data manipulation
          break;
        case 2:
          // View high scores
          // Use the view to display the high scores
          break;
        case 3:
          // View rules
          // Use the view to display the game rules
          break;
        default:
          // Invalid choice, handle accordingly
          break;
      }
    }

    // Quit the game
    view.displayExitMessage();
  }
}

