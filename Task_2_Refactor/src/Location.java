
public class Location {
  public int column;
  public int row;
  public DIRECTION d;
  
  public enum DIRECTION {VERTICAL, HORIZONTAL};
  
  public Location(int r, int c) {
    this.row = r;
    this.column = c;
  }

  public Location(int r, int c, DIRECTION d) {    
    this(r,c);
    this.d=d;
  }
  
  public String toString() {
    if(d==null){
      return "(" + (column+1) + "," + (row+1) + ")";
    } else {
      return "(" + (column+1) + "," + (row+1) + "," + d + ")";
    }
  }
}
