// Request class
class Game {
    private int score;

    public void increaseScore(int points) {
        score += points;
        System.out.println("Score increased by " + points + ". Total score: " + score);
    }

    public void decreaseScore(int points) {
        score -= points;
        System.out.println("Score decreased by " + points + ". Total score: " + score);
    }
}