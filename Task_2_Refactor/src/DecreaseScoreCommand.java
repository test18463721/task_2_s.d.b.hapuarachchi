// Concrete command classes
class DecreaseScoreCommand implements Command {
    private Game game;
    private int points;

    public DecreaseScoreCommand(Game game, int points) {
        this.game = game;
        this.points = points;
    }

    public void execute() {
        game.decreaseScore(points);
    }

    public void undo() {
        game.increaseScore(points);
    }
}