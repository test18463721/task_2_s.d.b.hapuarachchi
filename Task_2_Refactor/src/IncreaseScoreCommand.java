// Concrete command classes
class IncreaseScoreCommand implements Command {
    private Game game;
    private int points;

    public IncreaseScoreCommand(Game game, int points) {
        this.game = game;
        this.points = points;
    }

    public void execute() {
        game.increaseScore(points);
    }

    public void undo() {
        game.decreaseScore(points);
    }
}
